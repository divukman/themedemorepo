package theme.demo.com.themedemo;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Button mBtnScreenshots = null;
    private Button mBtnHowTo = null;
    private Button mBtnApplyTheme = null;
    private Button mBtnThemeMgr = null;

    private String [] mLstBtnOneIDs = {
            "screen1",
            "screen2",
            "screen3",
            "screen4",
            "screen5",
            "screen6",
            "screen7",
            "screen8"
    };

    private String [] mLstBtnTwo = {
            "howto"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        mBtnScreenshots = (Button) findViewById(R.id.btnScreenShots);
        mBtnHowTo = (Button) findViewById(R.id.btnHowToApply);
        mBtnApplyTheme = (Button) findViewById(R.id.btnApplyTheme);
        mBtnThemeMgr = (Button) findViewById(R.id.btnOpenThemeManager);
        addListeners();
    }

    private void addListeners() {
        mBtnScreenshots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Show gallery activity
                Intent myIntent = new Intent(MainActivity.this, GalleryActivity.class);
                myIntent.putExtra("IDs", mLstBtnOneIDs); //Optional parameters
                startActivity(myIntent);
            }
        });

        mBtnHowTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Show gallery activity
                Intent myIntent = new Intent(MainActivity.this, GalleryActivity.class);
                myIntent.putExtra("IDs", mLstBtnTwo); //Optional parameters
                startActivity(myIntent);
            }
        });

        mBtnApplyTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Copy file, show notification
                if (isStoragePermissionGranted()) {
                   copyFile();
                }

            }
        });


        mBtnApplyTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Copy file, show notification
                if (isStoragePermissionGranted()) {
                    copyFile();
                }

            }
        });


        mBtnThemeMgr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=U4WiyxXpyZc")));

            }
        });


    }

    private boolean copyFile() {
        final boolean result = FileUtil.copyFileFromAssets(MainActivity.this, "Theme.hwt");
        if (result) {
            Toast.makeText(MainActivity.this, "Theme successfully copied!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Failed to copy theme! Check your permissions!", Toast.LENGTH_SHORT).show();
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.nav_contact) {
//
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_browser) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
            startActivity(browserIntent);
        } else if (id == R.id.nav_send) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"support@themesinc.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, "Theme manager");
            i.putExtra(Intent.EXTRA_TEXT   , "I think your theme manager is great.");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_about) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    MainActivity.this);

            // set title
            alertDialogBuilder.setTitle("Themes Inc");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Themes Inc provides you with most beautiful themes for your smart phone!")
                    .setCancelable(false)
                    .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.dismiss();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v(MainActivity.class.getSimpleName(),"Permission: "+permissions[0]+ "was "+grantResults[0]);
            Toast.makeText(MainActivity.this, "Permission to access file system granted!", Toast.LENGTH_SHORT).show();
            copyFile();
        } else {
            Toast.makeText(MainActivity.this, "Permission to access file system denied!", Toast.LENGTH_SHORT).show();
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(MainActivity.class.getSimpleName(),"Permission is granted");
                return true;
            } else {

                Log.v(MainActivity.class.getSimpleName(),"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(MainActivity.class.getSimpleName(),"Permission is granted");
            return true;
        }
    }
}
