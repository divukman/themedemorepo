package theme.demo.com.themedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Gallery;

/**
 * Created by dvukman on 1/27/2017.
 */

@SuppressWarnings("deprecation")
public class GalleryActivity extends AppCompatActivity {

    private Gallery mGallery = null;
    private Button mBtnScreenshots = null;
    private String[] mLstImageIDs = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mLstImageIDs = intent.getStringArrayExtra("IDs");

        setContentView(R.layout.activity_gallery);
        mGallery = (Gallery) findViewById(R.id.gallery);
        mGallery.setAdapter(new ImageAdapter(GalleryActivity.this, mLstImageIDs));
    }
}
