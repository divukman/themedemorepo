package theme.demo.com.themedemo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.IOException;

/**
 * Created by dvukman on 1/27/2017.
 */

@SuppressWarnings("deprecation")
public class ImageAdapter extends BaseAdapter {
    private Context context;
    private int itemBackground;
    private String[] mImgNames;

    public ImageAdapter(Context c, final String []imgNames)
    {
        context = c;
        mImgNames = imgNames;
        // sets a grey background; wraps around the images
        TypedArray a = c.obtainStyledAttributes(R.styleable.MyGallery);
        itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
        a.recycle();
    }
    // returns the number of images
    public int getCount() {
        return mImgNames.length;
    }
    // returns the ID of an item
    public Object getItem(int position) {
        return position;
    }
    // returns the ID of an item
    public long getItemId(int position) {
        return position;
    }
    // returns an ImageView view
    public View getView(int position, View convertView, ViewGroup parent) {
        final String imgName = mImgNames[position];
        ImageView imageView = new ImageView(context);
        try {
            Drawable drawable = ImageUtil.getAssetImage(context, imgName);

            imageView.setLayoutParams(new Gallery.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            imageView.setBackground(drawable);
        } catch(IOException e) {
            Log.e("Error TAG", "Error loading image.");
        }
            return imageView;
    }
}