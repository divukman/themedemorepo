package theme.demo.com.themedemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

/**
 * Created by dvukman on 1/27/2017.
 */

public final class FileUtil {

    public static final String DIR = "HwThemes";//Todo
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    public static boolean copyFileFromAssets(final Context context, final String fileName) {
        boolean result = true;
        AssetManager am = context.getAssets();
        AssetFileDescriptor afd = null;
        try {
            afd = am.openFd(fileName);

            final String dirPath = Environment.getExternalStorageDirectory() + java.io.File.separator + DIR;
            File dir = new File(dirPath + java.io.File.separator);
            dir.mkdirs();


            File file = new File(dirPath + java.io.File.separator + fileName);
            file.createNewFile();

            copyFdToFile(afd.getFileDescriptor(), file);

        } catch (IOException e) {
            result = false; //todo log
            e.printStackTrace();
        }

        return result;
    }

    private static void copyFdToFile(FileDescriptor src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }


}
